import InputForm from './components/InputForm/InputForm.js'
import logo from './logo.svg';
import './App.css';
import React from 'react'

class App extends React.Component {
  constructor() {
    super()
    this.state = {
      input: '',
      name: ''
    }
  }

  onInputChange = (event) => {
    this.setState({input: event.target.value}, () => {
      console.log("Input: ", this.state.input)
    })
  }

  onButtonClick = () => {
     this.setState({name: this.state.input}, () => {
        console.log("Click:", this.state.name)
     });
  }

  render(){
    return (
      <div className="App">
        <InputForm onInputChange={this.onInputChange} onButtonClick={this.onButtonClick} name={this.state.name}/>
      </div>
    );
  }
}

export default App;
